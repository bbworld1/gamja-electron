const { app, BrowserWindow } = require('electron');
const serve = require('electron-serve');
const loadURL = serve({directory: 'gamja'});

(async () => {
  await app.whenReady();
  let win = new BrowserWindow({width: 800, height: 600});
  await loadURL(win);
})();

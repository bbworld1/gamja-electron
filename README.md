# gamja-electron: an Electron desktop client version of Gamja, emersion's IRC web client. #

This is an Electron desktop client wrapper around Gamja, emersion's minimal IRC web client.
It lets you run Gamja on the desktop.

NOTE: This was made as a joke! The entire project is completely antithecal to the original
purpose of Gamja, and was made because the dev was bored one day. Use gamja on the web,
or choose any one of the significantly less ridiculous desktop IRC clients out there.

But if you *really* insist on running it:

0. Clone this repo (`--recursive`)
1. `npm install`
2. `npm start` for dev mode
3. `npm run electron-pack` to build a production executable

Thanks to emersion (https://sr.ht/~emersion) for writing Gamja (https://sr.ht/~emersion/gamja)
and also not murdering me for making this.
